#In order to check the grammatical syntax of a sentence we import the Grammar PIP

import grammar_check
tool = grammar_check.LanguageTool('en-US')

#Taking a sentence by user input
sentence = raw_input("Enter a sentence:")

#Checking for Grammatical Errors
errors = tool.check(sentence)

#Splitting the sentence into a list of words separated by spaces
words = sentence.split()

#Number of spaces is 1 less than the number of words
spaces = len(words)-1

#Printing the corresponding message
if errors == 0 :
	print('No grammatical error found!')
else :
	print('Number of Grammatical Errors:',len(errors))

print('No. of spaces :',spaces)
